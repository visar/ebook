<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${activeLink == null}">
    <c:set var="activeLink" value="home" />
</c:if>

<div id="header_top">
    <%-- <img src="${pageContext.request.contextPath}/resources/images/logo.png" width="230" height="75" alt="" /> --%>

</div>

<div id="header_bottom">
    <div id="slogan">
        <p>Книгата е најдобриот другар!</p>
    </div> 
</div>  
