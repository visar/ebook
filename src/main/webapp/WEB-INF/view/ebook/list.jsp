<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- meta -->
<%@include file="../meta.jsp"%>

<body>
	<div id="wrapper">
        <!-- header -->
        <%@include file="../header.jsp"%>
		
		<div id="main_container">
		
            <!-- search -->
            <%@include file="../search.jsp"%>
			
			<div id="left">
				<div>
				
				    <h1>Книги</h1>
				    <br />
                    <table class="zebra">
                        <tr>
                         	<th> </th>
                            <th>Листа на книги</th>
                        </tr>

                        <tbody>
                            <c:set var="count" value="0" />
                            <c:forEach var="book" items="${bookList}">
                                <c:set var="count" value="${count + 1}" />
                                <tr>
                                    <td>${count}</td>
                                    <td><img src="${pageContext.request.contextPath}Repo/images/${book.id}.jpg" width="80" height="100"/></td>
                                    <td><a href="${pageContext.request.contextPath}/ebook/${book.id}">${book.isbn} ${book.title}</a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    
                    <br />
				<sec:authorize access="hasRole('ROLE_ADMIN')" >
                    <a class="link" href="${pageContext.request.contextPath}/ebook/new">Внеси</a>
                 </sec:authorize>
				</div>
				
				
			</div>	
		</div>
		
        <!-- footer -->
        <%@include file="../footer.jsp"%>
	</div>
	
</body>
</html>