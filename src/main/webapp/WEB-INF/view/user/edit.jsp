<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- meta -->
<%@include file="../meta.jsp"%>

<body>
	<div id="wrapper">
        <!-- header -->
        <%@include file="../header.jsp"%>
		
		<div id="main_container">
		
            <!-- search -->
            <%@include file="../search.jsp"%>
			
			<div id="left">
				<div>
				
				    <h1>Корисник</h1>
				    <br />
                    
                    <sf:form modelAttribute="user" method="post">
                    
                    	<c:if test="${action == 'update'}">                    		
                            <sf:input type="hidden" path="id" value="${user.id}" />
                            <sf:input type="hidden" path="optlock" value="${user.optlock}" />
                    	</c:if>
                    	
                        <p>                            
                 			Име <br />
                           	<c:if test="${action == 'new'}">
                           		<sf:input type="text" path="firstName" value="" />    
                           	</c:if>
                           	<c:if test="${action == 'update'}">
                            	<sf:input type="text" path="firstName" value="${user.firstName}" />    
                           	</c:if> 
                        </p>
                                           
                        <p>
                           Презиме <br />
                           <c:if test="${action == 'new'}">
                            	<sf:input type="text" path="lastName" value="" />    
                            </c:if>
                            <c:if test="${action == 'update'}">
                            	<sf:input type="text" path="lastName" value="${user.lastName}" />    
                            </c:if>
                        </p>
                        
						<p>
                           Корисничко име <br />
                           <c:if test="${action == 'new'}">
                            	<sf:input type="text" path="username" value="" />    
                            </c:if>
                            <c:if test="${action == 'update'}">
                            	<sf:input type="text" path="username" value="${user.username}" />    
                            </c:if>
                        </p>
                        
						<p>
                           Лозинка <br />
                           <c:if test="${action == 'new'}">
                            	<sf:input type="password" path="password" value="" />    
                            </c:if>
                            <c:if test="${action == 'update'}">
                            	<sf:input type="password" path="password" value="${user.password}" />    
                            </c:if>
                        </p>

						<p>
                           E-пошта <br />
                           <c:if test="${action == 'new'}">
                            	<sf:input type="text" path="email" value="" />    
                            </c:if>
                            <c:if test="${action == 'update'}">
                            	<sf:input type="text" path="email" value="${user.email}" />    
                            </c:if>
                        </p>
                                 
                		<p>
                           	Улоги <br />
                          	<sf:select path="userRoles" multiple="multiple">
   								<sf:option value="-1" label="--- Избери Улоги ---"/>
   								<sf:options itemValue="id" itemLabel="name" items="${userRoleList}" />
							</sf:select>
                        </p>
                                                                        
                    	<p>
                            <c:choose>
                                <c:when test="${action == 'new'}"> 
                                	<input type="submit" id="as_link" value="Внеси" />
                                </c:when>
                                <c:when test="${action == 'update'}">
                                    <input type="submit" id="as_link" value="Ажурирај" />
                                </c:when>
                            </c:choose>
                        
                        &nbsp; &nbsp;
                        <a class="link" href="${pageContext.request.contextPath}/admin/user/list">Корисници</a>
                        </p>
                    
                    </sf:form>
                    
                    
                    <br />

				</div>
				
				
			</div>	
		</div>
		
        <!-- footer -->
        <%@include file="../footer.jsp"%>
	</div>
	
</body>
</html>