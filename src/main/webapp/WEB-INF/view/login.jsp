<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- meta -->
<%@include file="meta.jsp"%>

<body>
	<div id="wrapper">
		<!-- header -->
		<%@include file="header_no_links.jsp"%>

		<div id="main_container">

			<div id="left">

				<h4>Најавете се</h4>
				<br />

				<form action="<s:url value="j_spring_security_check" />" method="post">
					<p>Корисничко име: <br />
					<input type="text" name="j_username" class="span12" value="" />
					</p>
					
					<p>Лозинка: <br />
					<input type="password" name="j_password" class="span12" value="" />
					</p>
					
					<p>
					<button type="submit" name="submit" id="btn as_link">Потврди</button>
					</p>
				</form>

				<br />
				<c:if test="${param.error == 'true'}">
					<p>Внесените податоци за корисничко име или лозинка, не се точни.</p>
				</c:if>

			</div>
		</div>

		<!-- footer -->
		<%@include file="footer.jsp"%>
	</div>

</body>
</html>