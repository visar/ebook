package my.ebooks.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "categories")
public class Category extends AbstractModel {
	private static final long serialVersionUID = 1L;

	@Column(name = "name", nullable = false, unique = true)
	private String name;

	public Category() {
		super();
	}

	public Category(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", this.id)
				.append("name", this.name).append("enabled", this.enabled)
				.toString();
	}

}
