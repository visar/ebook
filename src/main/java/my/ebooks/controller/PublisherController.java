package my.ebooks.controller;

import javax.validation.Valid;


import my.ebooks.model.Publisher;
import my.ebooks.service.PublisherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/admin/publisher")
public class PublisherController {

	private static final String PAGE_VIEW = "publisher";
	private static final String ACTIVE_LINK = "publishers";

	@Autowired
	private PublisherService publisherService;

	/*
	 * LIST
	 */

	@RequestMapping("list")
	public String list(Model model) {
		model.addAttribute("activeLink", ACTIVE_LINK);
		model.addAttribute(publisherService.list());
		return PAGE_VIEW + "/list";
	}

	/*
	 * NEW
	 */

	@RequestMapping(value = "new", method = RequestMethod.GET)
	public String showNewForm(Model model) {
		model.addAttribute("activeLink", ACTIVE_LINK);
		model.addAttribute("action", "new");
		model.addAttribute(new Publisher());
		return PAGE_VIEW + "/edit";
	}

	@RequestMapping(value = "new", method = RequestMethod.POST)
	public String save(@Valid Publisher publisher, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return PAGE_VIEW + "/edit";
		}
		publisherService.saveOrUpdate(publisher);
		return "redirect:/admin/" + PAGE_VIEW + "/" + publisher.getId();
	}

	/*
	 * VIEW
	 */

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String show(@PathVariable Long id, Model model) {
		Publisher publisher = publisherService.find(id);
		model.addAttribute("publisher", publisher);
		return PAGE_VIEW + "/view";
	}

	/*
	 * UPDATE
	 */

	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String showUpdateForm(@PathVariable Long id, Model model) {
		model.addAttribute("activeLink", ACTIVE_LINK);
		model.addAttribute("action", "update");
		model.addAttribute(publisherService.find(id));
		return PAGE_VIEW + "/edit";
	}

	@RequestMapping(value = "update/{id}", method = RequestMethod.POST)
	public String update(@PathVariable Long id, @Valid Publisher publisher,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return PAGE_VIEW + "/edit";
		}

		publisherService.saveOrUpdate(publisher);

		return "redirect:/admin/" + PAGE_VIEW + "/" + publisher.getId();
	}

	/*
	 * DELETE
	 */
	@RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable Long id, Model model) {
		publisherService.delete(publisherService.find(id));
		model.addAttribute("activeLink", ACTIVE_LINK);
		model.addAttribute(publisherService.list());
		return PAGE_VIEW + "/list";
	}

}
