package my.ebooks.controller;

import javax.validation.Valid;

import my.ebooks.model.Category;
import my.ebooks.service.CategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/admin/category")
public class CategoryController {

	private final static String PAGE_VIEW = "category";
	private final static String ACTIVE_LINK = "categories";

	@Autowired
	private CategoryService categoryService;

	/*
	 * LIST
	 */

	@RequestMapping("list")
	public String list(Model model) {
		model.addAttribute("activeLink", ACTIVE_LINK);
		model.addAttribute(categoryService.list());
		return PAGE_VIEW + "/list";
	}

	/*
	 * NEW
	 */

	@RequestMapping(value = "new", method = RequestMethod.GET)
	public String showNewForm(Model model) {
		model.addAttribute("activeLink", ACTIVE_LINK);
		model.addAttribute("action", "new");
		model.addAttribute(new Category());
		return PAGE_VIEW + "/edit";
	}

	@RequestMapping(value = "new", method = RequestMethod.POST)
	public String save(@Valid Category category, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return PAGE_VIEW + "/edit";
		}
		categoryService.saveOrUpdate(category);
		return "redirect:/admin/" + PAGE_VIEW + "/" + category.getId();
	}

	/*
	 * VIEW
	 */

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String show(@PathVariable Long id, Model model) {
		Category category = categoryService.find(id);
		model.addAttribute("category", category);
		return PAGE_VIEW + "/view";
	}

	/*
	 * UPDATE
	 */

	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String showUpdateForm(@PathVariable Long id, Model model) {
		model.addAttribute("activeLink", ACTIVE_LINK);
		model.addAttribute("action", "update");
		model.addAttribute(categoryService.find(id));
		return PAGE_VIEW + "/edit";
	}

	@RequestMapping(value = "update/{id}", method = RequestMethod.POST)
	public String update(@PathVariable Long id, @Valid Category category,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return PAGE_VIEW + "/edit";
		}

		categoryService.saveOrUpdate(category);

		return "redirect:/admin/" + PAGE_VIEW + "/" + category.getId();
	}

	/*
	 * DELETE
	 */
	@RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable Long id, Model model) {
		categoryService.delete(categoryService.find(id));
		model.addAttribute("activeLink", ACTIVE_LINK);
		model.addAttribute(categoryService.list());
		return PAGE_VIEW + "/list";
	}
}
