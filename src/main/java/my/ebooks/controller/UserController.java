package my.ebooks.controller;

import java.util.Set;

import javax.validation.Valid;

import my.ebooks.model.User;
import my.ebooks.service.UserRoleService;
import my.ebooks.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/admin/user")
public class UserController {

    private final static String PAGE_VIEW = "user";
    private final static String ACTIVE_LINK = "users";

    @Autowired
    private UserService userService;
    
    @Autowired
    private UserRoleService userRoleService;
    
    @InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Set.class, "userRoles", new CustomCollectionEditor(Set.class) {
			@Override
			protected Object convertElement(Object element) {
				Long id = null;
				if (element instanceof String && !("".equals((String) element))) {
					try {
						id = Long.parseLong((String) element);
					} catch (NumberFormatException e) {
						// TODO log the error
					}
				} else if (element instanceof Long) {
					id = (Long) element;
				}

				return (id != null) ? userRoleService.find(id) : null;
			}
		});
	
	}
    

    /*
     * LIST
     */
    @RequestMapping("list")
    public String list(Model model) {
    	model.addAttribute("activeLink", ACTIVE_LINK);
        model.addAttribute(userService.list());
        return PAGE_VIEW + "/list";
    }
    
    /*
     * NEW
     */
    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String showNewForm(Model model) {
    	model.addAttribute("activeLink", ACTIVE_LINK);
    	model.addAttribute("action", "new");
        model.addAttribute(new User());        
        model.addAttribute(userRoleService.list());
        return PAGE_VIEW + "/edit";
    }

    @RequestMapping(value = "new", method = RequestMethod.POST)
    public String save(@Valid User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return PAGE_VIEW + "/edit";
        }
        userService.saveOrUpdate(user);
        return "redirect:/admin/" + PAGE_VIEW + "/" + user.getId();
    }

    
    /*
     * VIEW
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String show(@PathVariable Long id, Model model) {
        User user = userService.find(id);
        model.addAttribute("user", user);
        return PAGE_VIEW + "/view";
    }

    /*
     * UPDATE
     */
    @RequestMapping(value = "update/{id}", method = RequestMethod.GET)
    public String showUpdateForm(@PathVariable Long id, Model model) {
    	model.addAttribute("activeLink", ACTIVE_LINK);
    	model.addAttribute("action", "update");
    	model.addAttribute(userService.find(id));        
        model.addAttribute(userRoleService.list());
        return PAGE_VIEW + "/edit";
    }
    
    @RequestMapping(value = "update/{id}", method = RequestMethod.POST)
    public String update(@PathVariable Long id, @Valid User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return PAGE_VIEW + "/edit";
        }

        userService.saveOrUpdate(user);
        
        return "redirect:/admin/" + PAGE_VIEW + "/" + user.getId();
    }
    

    /*
     * DELETE
     */
    @RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable Long id, Model model) {
        userService.delete(userService.find(id));
        model.addAttribute("activeLink", ACTIVE_LINK);
        model.addAttribute(userService.list());
        return PAGE_VIEW + "/list";
    }


}
