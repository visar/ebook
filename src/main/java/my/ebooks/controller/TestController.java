package my.ebooks.controller;

import java.util.List;

import my.ebooks.model.Author;
import my.ebooks.model.Book;
import my.ebooks.model.Category;
import my.ebooks.service.AuthorService;
import my.ebooks.service.CategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestController {

	@Autowired
	private AuthorService service;
	
	@Autowired
	private CategoryService categoryService;

	@RequestMapping("test")
	public String test(Model model) {
		Author author = service.find(25L);
		
		if (author == null) {
			author = new Author();
			author.setFirstName("First");
			author.setLastName("Last");
		}
		
		System.out.println(author);

		author.setFirstName(author.getFirstName() + author.getId());
		
		service.saveOrUpdate(author);
		
		/*model.put("userRoleList", service.list());

		Place place = new Place();
		// save
		place.setName("Prvo");
		placeService.saveOrUpdate(place);

		Long id = place.getId();

		System.out.println(id);
		// update
		place = placeService.find(id);
		place.setName("Vtoro");

		placeService.saveOrUpdate(place);*/

		return "author/list";
	}


	@RequestMapping("test/hql")
	public String testHQL() {
		Category category = categoryService.find(1L);
		List<Book> booksByCategory = categoryService.findAllBooksByCategory(category);
		
		
		for (Book book : booksByCategory) {
			System.out.println(book);
		}
		
		return "author/list";
	}
	
	
}
