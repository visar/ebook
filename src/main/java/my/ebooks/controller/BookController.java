package my.ebooks.controller;


import java.io.File;
import java.io.IOException;
import java.util.Set;

import javax.imageio.IIOException;
import javax.validation.Valid;

import my.ebooks.model.Book;
import my.ebooks.model.Publisher;
import my.ebooks.service.AuthorService;
import my.ebooks.service.BookService;
import my.ebooks.service.CategoryService;
import my.ebooks.service.PublisherService;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;


@Controller
@RequestMapping("/ebook")
public class BookController {

	private final static String PAGE_VIEW = "ebook";
	private final static String ACTIVE_LINK = "ebooks";

	@Autowired
	private PublisherService publisherService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private CategoryService categoryService;

	@Autowired
	private BookService bookService;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Set.class, "categories",
				new CustomCollectionEditor(Set.class) {
					@Override
					protected Object convertElement(Object element) {
						Long id = null;
						if (element instanceof String
								&& !("".equals((String) element))) {
							try {
								id = Long.parseLong((String) element);
							} catch (NumberFormatException e) {
								// TODO log the error
							}
						} else if (element instanceof Long) {
							id = (Long) element;
						}

						return (id != null) ? categoryService.find(id) : null;
					}
				});
		binder.registerCustomEditor(Set.class, "publishers",
				new CustomCollectionEditor(Set.class) {
					@Override
					protected Object convertElement(Object element) {
						Long id = null;
						if (element instanceof String
								&& !("".equals((String) element))) {
							try {
								id = Long.parseLong((String) element);
							} catch (NumberFormatException e) {
								// TODO log the error
							}
						} else if (element instanceof Long) {
							id = (Long) element;
						}

						return (id != null) ? publisherService.find(id) : null;
					}
				});
		binder.registerCustomEditor(Set.class, "authors",
				new CustomCollectionEditor(Set.class) {
					@Override
					protected Object convertElement(Object element) {
						Long id = null;
						if (element instanceof String
								&& !("".equals((String) element))) {
							try {
								id = Long.parseLong((String) element);
							} catch (NumberFormatException e) {
								// TODO log the error
							}
						} else if (element instanceof Long) {
							id = (Long) element;
						}

						return (id != null) ? authorService.find(id) : null;
					}
				});

	}

	/*
	 * LIST
	 */

	@RequestMapping("list")
	public String list(Model model) {
		model.addAttribute("activeLink", ACTIVE_LINK);
		model.addAttribute(bookService.list());
		return PAGE_VIEW + "/list";
	}

	/*
	 * NEW
	 */

	@RequestMapping(value = "new", method = RequestMethod.GET)
	public String showNewForm(Model model) {
		model.addAttribute("activeLink", ACTIVE_LINK);
		model.addAttribute("action", "new");
		model.addAttribute(new Book());
		model.addAttribute(categoryService.list());
		model.addAttribute(authorService.list());
		model.addAttribute(publisherService.list());

		return PAGE_VIEW + "/edit";
	}

	@RequestMapping(value = "new", method = RequestMethod.POST)
	public String save(@Valid Book book, BindingResult bindingResult, @RequestParam(value="coverImage", required=false) MultipartFile coverImage, @RequestParam(value="bookFileName", required=false) MultipartFile bookFileName) {
		/*if (bindingResult.hasErrors()) {
			return PAGE_VIEW + "/edit";
		}*/
		
		
		// bug fixing publisher
		Publisher publisher = publisherService.find(Long.parseLong(book.getPublisher().getName()));
		book.setPublisher(publisher);
		bookService.saveOrUpdate(book);
		
		
		// cover image saving
		try{
			if(!coverImage.isEmpty()){
				String coverImageName = book.getId() + ".jpg";
				saveImage(coverImageName, coverImage);// saving file
				book.setCoverImage(coverImageName); // correcting book info
			}
		}catch(IIOException e){
			bindingResult.reject(e.getMessage());
			return PAGE_VIEW + "/edit";
		}
		
		
		// book file saving
		try{
			if(!bookFileName.isEmpty()){
				String fullBookFileName = book.getId() + ".pdf";
				saveBookPdf(fullBookFileName, bookFileName);// saving file
				book.setBookFileName(fullBookFileName); // correcting book info
			}
		}catch(IIOException e){
			bindingResult.reject(e.getMessage());
			return PAGE_VIEW + "/edit";
		}
		
		// persist book into DB
		bookService.saveOrUpdate(book);
		return "redirect:/" + PAGE_VIEW + "/" + book.getId();
	}

	

	
	private void saveBookPdf(String string, MultipartFile bookFileName) throws IIOException {
		try{
			String webRootPath = "C:/Program Files/apache-tomcat-7.0.39/wtpwebapps/myEbooksRepo/ebooks/";
			
			File file = new File(webRootPath  + string);
			FileUtils.writeByteArrayToFile(file, bookFileName.getBytes());
		}catch(IOException e){
			throw new IIOException("Не можи да зачува слика", e);
		}		
	}

	private void saveImage(String string, MultipartFile coverImage) throws IIOException {
		try{
			String webRootPath = "C:/Program Files/apache-tomcat-7.0.39/wtpwebapps/myEbooksRepo/images/";
			
			File file = new File(webRootPath  + string);
			FileUtils.writeByteArrayToFile(file, coverImage.getBytes());
		}catch(IOException e){
			throw new IIOException("Не можи да зачува слика", e);
		}
		
	}

	/*
	 * VIEW
	 */

	

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String show(@PathVariable Long id, Model model) {
		Book book = bookService.find(id);
		model.addAttribute("book", book);
		return PAGE_VIEW + "/view";
	}

	/*
	 * UPDATE
	 */

	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String showUpdateForm(@PathVariable Long id, @Valid Book book, Model model) {
		model.addAttribute("activeLink", ACTIVE_LINK);
		model.addAttribute("action", "update");
		model.addAttribute(bookService.find(id));
		model.addAttribute(categoryService.list());
		model.addAttribute(authorService.list());
		model.addAttribute(publisherService.list());
		return PAGE_VIEW + "/edit";
	}

	@RequestMapping(value = "update/{id}", method = RequestMethod.POST)
	public String update(@PathVariable Long id, @Valid Book book,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return PAGE_VIEW + "/edit";
		}

		bookService.saveOrUpdate(book);

		return "redirect:/" + PAGE_VIEW + "/" + book.getId();
	}

	/*
	 * DELETE
	 */
	@RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable Long id, Model model) {
		bookService.delete(bookService.find(id));
		model.addAttribute("activeLink", ACTIVE_LINK);
		model.addAttribute(bookService.list());
		return PAGE_VIEW + "/list";
	}
}
