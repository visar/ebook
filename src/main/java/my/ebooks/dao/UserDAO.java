package my.ebooks.dao;

import my.ebooks.model.User;

public interface UserDAO extends GenericDAO<User> {

}
