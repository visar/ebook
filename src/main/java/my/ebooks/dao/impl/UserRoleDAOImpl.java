package my.ebooks.dao.impl;

import my.ebooks.dao.UserRoleDAO;
import my.ebooks.model.UserRole;

import org.springframework.stereotype.Repository;

@Repository("userRoleDAO")
public class UserRoleDAOImpl extends GenericDAOImpl<UserRole> implements UserRoleDAO {

}
