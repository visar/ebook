package my.ebooks.dao.impl;

import my.ebooks.dao.UserDAO;
import my.ebooks.model.User;

import org.springframework.stereotype.Repository;

@Repository("userDAO")
public class UserDAOImpl extends GenericDAOImpl<User> implements UserDAO {

}
