package my.ebooks.dao;

import my.ebooks.model.UserRole;

public interface UserRoleDAO extends GenericDAO<UserRole> {

}
