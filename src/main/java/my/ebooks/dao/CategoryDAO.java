package my.ebooks.dao;

import java.util.List;

import my.ebooks.model.Book;
import my.ebooks.model.Category;

public interface CategoryDAO extends GenericDAO<Category> {
	public List<Book> findAllBooksByCategory(Category category);

}
