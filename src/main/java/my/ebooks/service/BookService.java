package my.ebooks.service;

import my.ebooks.model.Book;

public interface BookService extends GenericService<Book> {

}
