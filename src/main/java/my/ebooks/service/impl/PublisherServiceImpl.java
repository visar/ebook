package my.ebooks.service.impl;

import java.io.Serializable;
import java.util.List;

import my.ebooks.dao.PublisherDAO;
import my.ebooks.model.Publisher;
import my.ebooks.service.PublisherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("publisherService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PublisherServiceImpl implements PublisherService {
	@Autowired
	private PublisherDAO publisherDAO;

	@Override
	public Publisher find(Serializable id) {
		if (id == null) {
			return null;
		}
		return publisherDAO.find(id);
	}

	@Override
	public List<Publisher> list() {
		return publisherDAO.list();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveOrUpdate(Publisher publisher) {
		if (publisher != null) {
			publisherDAO.saveOrUpdate(publisher);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(Publisher publisher) {
		if (publisher != null) {
			publisherDAO.delete(publisher);
		}
	}

}
