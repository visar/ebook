package my.ebooks.service;

import my.ebooks.model.User;

public interface UserService extends GenericService<User> {

}
