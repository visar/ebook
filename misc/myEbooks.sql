-- user roles
INSERT INTO user_roles(id, enabled, optlock, name) VALUES (1, true, 0, 'ROLE_ADMIN');
INSERT INTO user_roles(id, enabled, optlock, name) VALUES (2, true, 0, 'ROLE_USER');
INSERT INTO user_roles(id, enabled, optlock, name) VALUES (3, true, 0, 'ROLE_GUEST');

-- categories
insert into categories (id, enabled, optlock, name) values (1, true, 0, 'Java');
insert into categories (id, enabled, optlock, name) values (2, true, 0, 'HTML');
insert into categories (id, enabled, optlock, name) values (3, true, 0, 'DB');

-- books

insert into books (id, enabled, optlock, book_file, copyright_year, cover_image, edition_number, isbn, pages, price, title) 
  values (1, true, 0, 'book.pdf', 2010, 'cover.png', 1, '1234-12-12-45', 400, 600, 'Hibernate from Scratch...');
  
  
-- books - categories

  insert into books_categories (books_id, categories_id) values (1, 1);